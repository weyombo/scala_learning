import scala.collection.parallel.immutable.ParVector
import scala.collection.parallel.immutable._
object test{
 val v = Vector.range(0,10)

  v.par.foreach{
    e =>print(e);
      Thread.sleep(100)
  }

  val f = ParVector.range(0,10)
  f.foreach{
    e=>print(e)
      Thread.sleep(1000)
  }

  def f(s : String) =  "f(" + s + ")"

  def g(s: String) = "g(" + s + ")"

  val fComposeG = f _ compose g _


}





