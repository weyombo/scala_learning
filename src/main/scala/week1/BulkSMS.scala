package  week1
import java.net.URLEncoder

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model._

import scala.concurrent.Future
import scala.util.{Failure, Success}


object BulkSMS {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem()
    implicit val executionContext = system.dispatcher
    var message: String = URLEncoder.encode("Some Message to send", "UTF-8")
    var phoneNumbers: String = URLEncoder.encode("+254722268163")
    val apiKey: String = "92364da02bbadc3e13bbf797be8ea2d5ba3e99fba1e059bcb318c746503f5328";
    val Apikey = headers.Accept()
    val url: String = "http://api.africastalking.com/version1/messaging"
    val responseFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(method = POST, uri = url))

    responseFuture
      .onComplete {
        case Success(res) => println(res)
        case Failure(e) => e.getStackTrace
      }
  }

}
