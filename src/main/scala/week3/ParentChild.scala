package week3
import akka.actor._

case class CreateChild(name : String)
case class Name(name :String)

class Child extends Actor{
  var name = "No name"
  override def postStop = println(s"Someone murdered me ($name):${self.path}")

  def receive = {
    case Name(name) => this.name=name
    case _ => println(s"Child $name got somethin'")
  }
}

class Parent extends Actor{
  def receive={
    case CreateChild(name) =>
      println(s"Parent getting ready to create babies !!!!000!!!!. Then name, if successfull will be ($name)")
      val child = context.actorOf(Props[Child],name=s"$name")
      context.watch(child)
      child ! Name(name)
    case Terminated(name) => println(s"Sob Sob Sob, one of my children has been killed. RIP $name")
    case _ => println("I gat nothin' to do other than make babies")
  }
}

object ParentChild extends App{

  val system = ActorSystem("parentMakingChildren")
  val parent = system.actorOf(Props[Parent], name="parent")

  parent ! CreateChild("Karl")
  parent ! CreateChild("Benz")

  Thread.sleep(500)

  //looking for child benz and kill them i.e PoisonPill
  val benz = system.actorSelection("/user/parent/Benz")
  benz ! PoisonPill
  println("Benz swallowed the poison, and died, instantly, maybe he struggled for a few minutes, i dont know, but he died")
   Thread.sleep(5000)

  system.terminate()

}
