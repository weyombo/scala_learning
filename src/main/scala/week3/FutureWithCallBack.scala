package week3

import scala.concurrent.Future
import scala.util.{Failure, Random, Success}
import scala.concurrent.ExecutionContext.Implicits.global

object FutureWithCallBack extends App {

  println("Starting Calculation")
  val f = Future{
    Thread.sleep(Random.nextInt(500))
    if(Random.nextInt(500)<250) throw new Exception("What the helllro") else 1+1
  }
  println("before onComplete")
  f.onComplete{
    case Success(value) =>println(s"Got the callback, meaning = $value")
    case Failure (e) => e.printStackTrace
  }

  f onSuccess {
    case result => println(s"Success: $result")
  }

  f onFailure {
    case t => println(s"Exception: ${t.getMessage}")
  }
  //do some other work
  println("A ........");Thread.sleep(100)
  println("B ........");Thread.sleep(100)
  println("C ........");Thread.sleep(100)
  println("D ........");Thread.sleep(100)
  println("E ........");Thread.sleep(100)
  println("F ........");Thread.sleep(100)
  Thread.sleep(2000)

}
