package week3

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object FutureWithMethod extends App {

  def f(i: Int): Future[Int] = Future{
      Thread.sleep(100)
    i+1
  }
  f(11).onComplete{
    case Success(result) => println(s"result = $result")
    case Failure(e) =>println(e.getStackTrace)
  }

  Thread.sleep(1000)

}
