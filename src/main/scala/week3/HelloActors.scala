package week3

import akka.actor.{Actor, ActorSystem, Props}

class HelloActors(input: String) extends Actor {
  def receive = {
    case "Hello" => println(s"Hello $input")
    case _ => println(s"What are you doing here $input ?")
  }
}

object HelloActors extends App{
  //actor nees an actor system
  val system = ActorSystem("HelloAcca")
  //create and start the actor
  val helloActor = system.actorOf(Props(new HelloActors("Glen")),name = "helloacca")

  //sennd a message to the actor
  helloActor ! "Hello"
  helloActor ! "hello"
  helloActor ! "die"
  system.terminate()
}
