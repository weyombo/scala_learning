package week3
import net.liftweb.json.JsonDSL._
import net.liftweb.json._



case class PersonWithFriends(name:String, address:Address1){
  var friends = List[PersonWithFriends]()
}
case class Address1(state: String, city: String)
object JsonWithLists extends App{
  implicit val formats = DefaultFormats
    val p1 = PersonWithFriends("Friend one",Address1("Kahawa","Sukari"))
    val p2 = PersonWithFriends("Friend two",Address1("Kahawa","West"))
    val friends = List(p1,p2)
    val me = PersonWithFriends("Friends Owner",Address1("Drive","Lumumba"))
    me.friends = friends
    val json = (
      "person" ->
        ("name" -> me.name)~
          addressFun(me.address)~
            friendsFun(me)

    )

  def addressFun(i: Address1) = {
    (
      "address" ->
        ("city" -> i.city)~
          ("state" -> i.state)
      )
  }
  def friendsFun(fri :PersonWithFriends) = {
    (
      "friends" ->
        fri.friends.map{
          f =>
            ("name" -> f.name)~
              (
                "address" ->
                  ("city" -> f.address.city)~
                    ("state" -> f.address.state)
                )
        }
      )
  }

  println(compactRender(json))
}
