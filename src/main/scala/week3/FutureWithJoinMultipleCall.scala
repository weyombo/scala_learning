package week3

import scala.concurrent.Future
import scala.util.Random
import scala.concurrent.ExecutionContext.Implicits.global

object Cloudmimik{
  def runAlgorithm(i: Int): Future[Int] = Future{
    Thread.sleep(Random.nextInt(500))
    val result = i + 12
    println(s"returning result from cloud: $result")
    result
  }
}

object FutureWithJoinMultipleCall extends App{
   println("Something crazy is about to go down")
  val result1 = Cloudmimik.runAlgorithm(12)
  val result2 = Cloudmimik.runAlgorithm(50)
  val result3 = Cloudmimik.runAlgorithm(13)

  val result = for{
    r1 <- result1
    r2 <- result2
    r3 <- result3
  }yield (r1+r2+r3)

  println("before success")
  result onSuccess{
    case result => println(s"total from Cloudmimik is : $result")
  }

  println("before sleep at the end")
  Thread.sleep(2000)

}
