package week3

import com.google.gson.GsonBuilder

case class Human(name: String, address: Address){
  var friends: Array[Human] = _
}
object JsonWithArray extends App {
  val me = Human("Glen Weyombo",Address("Kila","Place"))
  val merc = Human("Mercedes Benz",Address("hapa","tu"))
  val merc1 = Human("Best or Nothing",Address("Stuttgart","Germany"))
  val fn = Array(merc,merc1)
  me.friends = fn
  val gson = (new GsonBuilder()).setPrettyPrinting().create();
  println(gson.toJson(me))
}
