package week3
import scala.concurrent.{Await,Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object FutureWithBlock extends App {

  implicit val baseTime = System.currentTimeMillis()

  //create a future
  val f = Future{
    Thread.sleep(500)
    1 + 1
  }

  //blocking
  val result = Await.result(f,1 second)
  println(result)

  Thread.sleep(1000)

}
