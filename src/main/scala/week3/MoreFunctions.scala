
import akka.actor._

class Glen extends Actor{
  override def preStart() { println("prestart function called")}
  override def postStop(){println("Post stop function called")}

  override def preRestart(reason: Throwable, message: Option[Any]) ={
    println("Pre Restart")
    println(s"Message: ${message.getOrElse("")}")
    println(s" Reason: ${reason.getMessage}")
    super.preRestart(reason,message)
  }

  override def postRestart(reason: Throwable): Unit = {
    println("Post restart")
    println(s" Reason : ${reason.getMessage}")
    super.postRestart(reason)
  }
  def receive = {
    case ForcedRestart => throw new Exception("Booooooooom!")
    case _ => println("Actor received a message")
  }
}

case object ForcedRestart

object MoreFunctions extends App{
  val system = ActorSystem("otherFunctions")
  val glenfunctions = system.actorOf(Props[Glen],name = "whateverNameYouWannaCallYourself")

  glenfunctions ! "hello"
  Thread.sleep(1000)
  println("forcing a restart")

  glenfunctions ! ForcedRestart
  Thread.sleep(1000)

  //stopping the actor
  system.stop(glenfunctions)

  //shutdown
  system.terminate()

}