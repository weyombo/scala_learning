package week3

import akka.actor._

case object ActNormalMessage
case object TryToFindSolution
case object BasGysMakeMeAngry

class ActorSwitcher extends Actor{
  import context._

  def angryState: Receive ={
    case ActNormalMessage =>
      println(" I'm acting normal")
      become(normalState)
  }

  def normalState: Receive ={
    case TryToFindSolution =>
      println("Looking for solutions to my problem")
    case BasGysMakeMeAngry =>
      println("Bad guys make me angry, mean guys are cool though")
      become(angryState)
  }

  def receive = {
    case BasGysMakeMeAngry => become(angryState)
    case ActNormalMessage => become(normalState)
  }
}

object ActorSwitchState extends App {
  val system = ActorSystem("BecomeHulkExample")
  val hulkActor = system.actorOf(Props[ActorSwitcher], name = "HulkBanner")

  hulkActor ! ActNormalMessage
  hulkActor ! TryToFindSolution
  hulkActor ! BasGysMakeMeAngry
  Thread.sleep(1000)
  hulkActor ! ActNormalMessage

  system.terminate()


}
