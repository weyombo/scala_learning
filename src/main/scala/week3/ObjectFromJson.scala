package week3
import net.liftweb.json._
object ObjectFromJson extends App {
  implicit val formats = DefaultFormats

  val jsonString = "{\"name\":\"Mercedes Benz\",\"address\":{\"city\":\"Stuttgart\",\"state\":\"Germany\"}}"
  val jValue = parse(jsonString)
  val personServer = jValue.extract[Person]
  println("Name: "+personServer.name)
  println("My city: "+personServer.address.city)
  println("Our State: "+personServer.address.state)

}
