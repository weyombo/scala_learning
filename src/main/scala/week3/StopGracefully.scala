package week3

import akka.actor._
import akka.pattern.gracefulStop

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global


class ToBeStopped extends Actor{
  def receive = {
    case _ => println("My mission is to get killed, nothing more, nothing less")
  }
}

object StopGracefully extends App{
    val system = ActorSystem("killerActor")
    val dyieeActor = system.actorOf(Props[ToBeStopped],name="dyiee")
  try{
    val  stopped: Future[Boolean] = gracefulStop(dyieeActor,2 seconds)
    val result = Await.result(stopped, 3 seconds)

    if(result)println("Dyiee accomplished it's purpose")
  }catch {
    case e:Exception => e.printStackTrace
  }finally {
    system.terminate()
  }
}
