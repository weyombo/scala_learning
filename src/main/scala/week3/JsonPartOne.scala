package week3

import com.google.gson.Gson

case class Person(name: String, address: Address)
case class Address(city: String,state:String)

object JsonPartOne extends App {
  val p = Person("Mercedes Benz",Address("Stuttgart","Germany"))
  val gson = new Gson
  val jsonString = gson.toJson(p)
  println(jsonString)
}
