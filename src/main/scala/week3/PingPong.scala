package week3
import akka.actor._

case object StartMessage
case object StopMessage
case object PingMessage
case object PongMessage

class Ping (pong: ActorRef) extends Actor {
  var count = 0
  def incrementAndPrint {count +=1; println(s"ping, the count is: $count")}
  def receive={
    case StartMessage =>
      println(s"System starting at count: $count")
      incrementAndPrint
      pong ! PingMessage
    case PongMessage =>
      incrementAndPrint
      if(count > 99){
        sender ! StopMessage
        println(s"Ping stopped at count: $count")
        context.stop(self)
      }else{
        sender ! PingMessage
      }
    case _ => println("Ping got something unexpected")
  }
}

class Pong extends Actor{
  def receive = {
    case PingMessage =>
      println("pong")
      sender ! PongMessage
    case StopMessage =>
      println("Pong stopped")
      context.stop(self)
    case _ => println("Pong received unexpected Message")
  }
}


object PingPong extends App{
  val system = ActorSystem("pingpongsystem")
  val pong = system.actorOf(Props[Pong], name="pong")
  val ping = system.actorOf(Props(new Ping(pong)), name = "ping")
  ping ! StartMessage
}
