package week3

import org.scalatest.FunSuite

class CalculatorTest extends FunSuite{
    test("Calculator.cube"){
      assert(Calculator.cube(3) === 27)
    }
}
