name := "bulkSMS"

version := "0.1"

scalaVersion := "2.12.4"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.9"
libraryDependencies += "com.typesafe.akka" %% "akka-http-core" % "10.0.11"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.9"
libraryDependencies += "net.liftweb" %% "lift-json" % "3.2.0"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.10"
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.2"
libraryDependencies += "net.liftweb" %% "lift-json" % "3.2.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

